FROM ruby:2.7.0
RUN mkdir quiz-app
RUN cd quiz-app
RUN curl https://deb.nodesource.com/setup_12.x | bash
RUN curl https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -qq -y --no-install-recommends bash curl nodejs yarn
ADD Gemfile Gemfile.lock ./

RUN bundle install -j8 -r4
EXPOSE 3004

COPY ./ ./
RUN yarn install
COPY docker-entrypoint.sh docker-entrypoint.sh
RUN chmod +x docker-entrypoint.sh
ENTRYPOINT ["sh","docker-entrypoint.sh"]

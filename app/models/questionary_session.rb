class QuestionarySession < ApplicationRecord
  belongs_to :questionary
  belongs_to :current_question, class_name: 'Question', optional: true
  has_many :questions, through: :questionary
  has_many :answers

  enum state: { ongoing: 0, done: 1}

  delegate :title, to: :questionary

  def get_question
    if self.current_question.blank?
      self.current_question = self.questions.first
      self.save
    end

    self.current_question
  end

  def total_questions
    self.questions.size
  end

  def current_question_number
    self.question_ids.index(get_question.id)
  end

  def last_question?
    current_question_number == total_questions - 1
  end

  def next_question!
    new_question = questions[current_question_number + 1]
    self.current_question = new_question
    self.save
    self.current_question
  end

  def create_answer(question, answer)
    self.answers.create(
      question: question,
      answer: answer,
      correct: answer == question.correct_answer
    )
  end
end

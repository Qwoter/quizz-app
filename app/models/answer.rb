class Answer < ApplicationRecord
  belongs_to :question
  belongs_to :questionary_session

  delegate :questinary, to: :question

  def self.aggregate_answers_for_graph
    includes(:questionary_session).
      where(questionary_sessions: {state: :done}).
      group(:answer).
      count
  end
end

class Questionary < ApplicationRecord
  has_many :questions
  has_many :questionary_sessions
  has_many :answers, through: :questionary_sessions
end

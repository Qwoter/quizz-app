class Question < ApplicationRecord
  belongs_to :questionary
  has_many :answers
  has_many :questionary_session

  def select_answers_for_graph
    zero_hash = {}
    possible_answers.size.times do |i|
      zero_hash[possible_answers[i]] = 0
    end

    all_answers = self.answers.aggregate_answers_for_graph
    return nil if all_answers.sum {|k, v| v} == 0

    all_answers.each do |k, v|
      zero_hash[possible_answers[k]] = v
    end

    zero_hash
  end
end

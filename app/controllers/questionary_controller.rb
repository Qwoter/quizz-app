class QuestionaryController < ApplicationController
  def index
    prepare_questionary_session
  end

  def next
    if answer_params.blank?
      flash[:notice] = 'Please Select Answer'
      return redirect_to '/'
    end

    prepare_questionary_session
    @questionary_session.create_answer(@question, answer_params['answer'].to_i)

    if @questionary_session.last_question?
      @questionary_session.done!
      redirect_to '/graph'
    else
      @question = @questionary_session.next_question!
      redirect_to '/'
    end
  end

  def graph
  end

  private

  def prepare_questionary_session
    if session['questionary_session_id'].present?
      @questionary_session = QuestionarySession.find(session['questionary_session_id'].to_i)
    else
      @questionary_session = QuestionarySession.create(questionary: Questionary.first)
      session['questionary_session_id'] = @questionary_session.id
    end

    if @questionary_session.done?
      flash[:notice] = 'Quiz was successfully finished'
      redirect_to '/graph'
    end

    @question = @questionary_session.get_question
  end

  def answer_params
    params.permit(:answer)
  end
end

# Quizz App

## Starting
There are 2 ways to run this app:

### How to run: First Method (recommended)

#### Through docker for mac (or docker for linux)

1. Clone project and go inside
    ```bash
    git clone git@bitbucket.org:Qwoter/quizz-app.git
    cd quizz-app
    ```
    
2. Download and install docker [docker for mac](https://download.docker.com/mac/stable/Docker.dmg) ([how to install]([https://docs.docker.com/docker-for-mac/](https://docs.docker.com/docker-for-mac/)))
    *skip 3 if you have docker installed*
    
3. Allow Docker to mount cloned code into docker container in


    ```
    Docker->Preferences->Resources->File Sharing
    ``` 


5. Go into dock folder inside quizz-app project `cd dock`

6. Run `docker-compose build && docker-compose up` it will complete in **~10 minutes**


    - it will download and install ruby 2.7 container, setup missing libraries
    - then it will setup rails app by running `bundle install` and `yarn install` and run app server by executing `docker-entrypoint.sh`
    
    
7. You should see:


```bash
quiz-app_1  | => Booting Puma
quiz-app_1  | => Rails 6.0.2.1 application starting in development
quiz-app_1  | => Run `rails server --help` for more startup options
quiz-app_1  | Puma starting in single mode...
quiz-app_1  | * Version 3.12.2 (ruby 2.7.0-p0), codename: Llamas in Pajamas
quiz-app_1  | * Min threads: 5, max threads: 5
quiz-app_1  | * Environment: development
quiz-app_1  | * Listening on tcp://0.0.0.0:3004
```
    
    
- Project should be available on [http://127.0.0.1:3005/](http://127.0.0.1:3005/)

### How to run: Second Method


1. Clone project and go inside
    ```bash
    git clone git@bitbucket.org:Qwoter/quizz-app.git
    cd quizz-app
    ```
2. Install all Rails dependencies: ruby 2.7, nodejs, yarn, bundle etc
3. Run `bundle install` then `yarn install` then `rails s`
4. Project should be available on [http://127.0.0.1:3000/](http://127.0.0.1:3000/)


## Tests
- Inside project folder run `RUBYOPT=-W:no-deprecated rspec` and you will see results.
- In case of docker setup first go inside docker then run before mentioned command  there.
- To get inside docker container: `docker ps` -> copy `CONTAINER ID` of `dock_quiz-app` image then run `docker exec -ti (CONTAINER_ID) bash`.

## DB
- SQLite DB is already inside repository so you don't need to create, migrate or seed anything
- Test DB is also ready to go

## Tips
- Once you pass the quizz if you want to do it again delete/rename cookie `_quiz_app_session` in chrome console
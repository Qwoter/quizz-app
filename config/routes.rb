Rails.application.routes.draw do
  root to: 'questionary#index', as: 'root'
  post '/next', to: 'questionary#next', as: 'next'
  get '/graph', to: 'questionary#graph', as: 'graph'
end

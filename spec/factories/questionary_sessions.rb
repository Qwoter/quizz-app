FactoryBot.define do
  factory :questionary_session do
    state { 'ongoing' }
    questionary { Questionary.first || create(:questionary) }
    current_question_id { 0 }
  end
end

FactoryBot.define do
  factory :questionary do
    title { "Some questionary" }
    questions { create_list(:question, 3, questionary: Questionary.first) }
  end
end

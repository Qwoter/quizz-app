FactoryBot.define do
  factory :question do
    possible_answers { ['ans 1', 'ans 2', 'ans 3'] }
    correct_answer { 0 }
    description { 'Description of the question' }
    association :questionary
  end
end

FactoryBot.define do
  factory :answer do
    answer { 0 }
    correct { false }
    question { create(:question) }
    questionary_session { create(:questionary_session) }
  end
end

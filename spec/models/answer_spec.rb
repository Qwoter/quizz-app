require 'rails_helper'

RSpec.describe Answer, type: :model do
  it 'can be created' do
    answer = create(:answer)
    expect(answer.valid?).to eq(true)
  end
end

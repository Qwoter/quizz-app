require "rails_helper"

RSpec.describe QuestionaryController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/").to route_to("questionary#index")
    end

    it "routes to #graph" do
      expect(:get => "/graph").to route_to("questionary#graph")
    end
  end
end

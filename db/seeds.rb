# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

questionary = Questionary.create(title: 'Psychological Questionary')
pa1 = ['0-9', '10-24', '25-49', '50+']
pa2 = ['Game of thrones', 'Shrek 2', 'The Shawshank Redemption', 'Pulp Fiction']
pa3 = ['Yes', 'No', 'Maybe', 'My Psychiatrist knows the answer']
pa4 = ['Game of thrones', 'Lord of the rings', 'Rails way', 'Math for dummies']
pa5 = ['Water Bear', 'Tardigrade', 'A robot', 'Mermaid', 'Human']
q1 = Question.create(description: 'How old are you?', possible_answers: pa1, correct_answer: 0, questionary: questionary)
q2 = Question.create(description: 'What is your favorite movie?', possible_answers: pa2, correct_answer: 3, questionary: questionary)
q3 = Question.create(description: 'Do you fly while asleep?', possible_answers: pa3, correct_answer: 3, questionary: questionary)
q4 = Question.create(description: 'What is your favorite book?', possible_answers: pa4, correct_answer: 1, questionary: questionary)
q5 = Question.create(description: 'Tell me who your are', possible_answers: pa5, correct_answer: 2, questionary: questionary)

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_09_113359) do

  create_table "answers", force: :cascade do |t|
    t.integer "answer"
    t.boolean "correct"
    t.integer "question_id"
    t.integer "questionary_session_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["question_id"], name: "index_answers_on_question_id"
    t.index ["questionary_session_id"], name: "index_answers_on_questionary_session_id"
  end

  create_table "questionaries", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "questionary_sessions", force: :cascade do |t|
    t.integer "state", default: 0, null: false
    t.integer "questionary_id"
    t.integer "current_question_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["current_question_id"], name: "index_questionary_sessions_on_current_question_id"
    t.index ["questionary_id"], name: "index_questionary_sessions_on_questionary_id"
    t.index ["state"], name: "index_questionary_sessions_on_state"
  end

  create_table "questions", force: :cascade do |t|
    t.json "possible_answers"
    t.integer "correct_answer"
    t.text "description"
    t.integer "questionary_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["questionary_id"], name: "index_questions_on_questionary_id"
  end

end

class Answers < ActiveRecord::Migration[6.0]
  def change
    create_table :answers do |t|
      t.integer :answer
      t.boolean :correct
      t.belongs_to :question
      t.belongs_to :questionary_session
      t.timestamps
    end
  end
end

class Questions < ActiveRecord::Migration[6.0]
  def change
    create_table :questions do |t|
      t.json :possible_answers
      t.integer :correct_answer
      t.text :description
      t.belongs_to :questionary
      t.timestamps
    end
  end
end

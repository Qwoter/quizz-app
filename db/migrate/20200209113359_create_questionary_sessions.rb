class CreateQuestionarySessions < ActiveRecord::Migration[6.0]
  def change
    create_table :questionary_sessions do |t|
      t.integer :state, index: true, null: false, default: 0
      t.belongs_to :questionary
      t.belongs_to :current_question
      t.timestamps
    end
  end
end

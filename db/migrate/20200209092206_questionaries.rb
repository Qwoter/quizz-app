class Questionaries < ActiveRecord::Migration[6.0]
  def change
    create_table :questionaries do |t|
      t.string :title
      t.timestamps
    end
  end
end
